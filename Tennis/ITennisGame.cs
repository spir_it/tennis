﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/*
 * class Player - because he can play in turnament but I don't want to touch ITennisGame interface => Player handler is inside TennisGame class,
 *              in such a model it could be ommited becouse there is no any other attribute that should be stored in this class but it suits here becaouse of object orineted modeling 
 * class Score - score readable for human
 * class ScoreBuilder - .WithDraw("") .WithAdventage(playerName), .WithWinFor(playerName), .WithRegular("")
 * class ScoreHistory - 
 * interface ITennisGameScoringStrategy
 * enum na wynik
 * class TennisGameFactory - returns tennis game with particular TennisGameScoringStrategy
 * class Point - needles becaouse now there is no any other given attribute that could be stored in this class
 * class PointsBoard 
 *
 * Should I assume that it is a part of bigger system for example TurnamentTennisGame ?
 */
namespace Tennis
{
    public interface ITennisGame
    {
        void WonPoint(string playerName);
        string GetScore();
    }
}
