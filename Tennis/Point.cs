﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public class Point
    {
        public int Value { get; private set; }

        public Point()
        {
            Value = 0;
        }

        public Point(int value)
        {
            Value = value;
        }

        // Can also be an overloaded operator++
        public void Increase()
        {
            Value++;
        }

        public string ToWord()
        {
            return Value < 4 ? (new String[] {"Love", "Fifteen", "Thirty", "Forty"})[Value] : String.Empty;
        }

        // ToString() instead of ToWord()
        // operator>
        // operator<
        // operator==
        // operator++
    }
}
