﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public class PointsBoard
    {
        private Dictionary<string, Player> _pointsBoard = new Dictionary<string, Player>();

        public PointsBoard(Player player1, Player player2)
        {
            _pointsBoard[player1.Name] = player1;
            _pointsBoard[player2.Name] = player2;
        }

        public void AddPoint(string playerName)
        {
            _pointsBoard[playerName].AddPoint();
        }
    }
}
