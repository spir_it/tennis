﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tennis.ComputeStrategies;

namespace Tennis
{
    public class TennisGameFactory
    {
        private string _player1Name;
        private string _player2Name;

        public TennisGameFactory(string player1Name, string player2Name)
        {
            _player1Name = player1Name;
            _player2Name = player2Name;
        }

        public ITennisGameExtended GetTennisGameWithFirstComputeScoreStrategy()
        {
            return GetTennisGameWithComputeScoreStrategy(new FirstComputeScoreStrategy());
        }

        private ITennisGameExtended GetTennisGameWithComputeScoreStrategy(IComputeScoreStrategy computeScoreStrategy)
        {
            return new TennisGame(_player1Name, _player2Name, computeScoreStrategy);
        }

        public ITennisGameExtended GetTennisGameWithSecondComputeScoreStrategy()
        {
            return GetTennisGameWithComputeScoreStrategy(new SecondComputeScoreStrategy());
        }

        public ITennisGameExtended GetTennisGameWithThirdComputeScoreStrategy()
        {
            return GetTennisGameWithComputeScoreStrategy(new ThirdComputeScoreStrategy());
        }
    }
}
