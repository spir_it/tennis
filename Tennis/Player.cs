﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public class Player
    {
        public string Name { get; }

        public Point Points { get; private set; }

        public Player(string name)
        {
            Name = name;
            Points = new Point(0);
        }

        public void AddPoint()
        {
            Points.Increase();
        }
    }
}
