﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tennis.ComputeStrategies;

namespace Tennis.Tests
{

    [TestFixture]
    public class ExampleGameTennisTest
    {
        private readonly TennisGameFactory _tennisGameFactory;

        public ExampleGameTennisTest()
        {
            _tennisGameFactory = new TennisGameFactory(TennisGameDefaults.Player1Name, TennisGameDefaults.Player2Name);
        }

        public void RealisticTennisGame(ITennisGame game)
        {
            String[] points = { TennisGameDefaults.Player1Name, TennisGameDefaults.Player1Name, TennisGameDefaults.Player2Name, TennisGameDefaults.Player2Name, TennisGameDefaults.Player1Name, TennisGameDefaults.Player1Name };
            String[] expected_scores = { "Fifteen-Love", "Thirty-Love", "Thirty-Fifteen", "Thirty-All", "Forty-Thirty", $"Win for {TennisGameDefaults.Player1Name}" };
            for (int i = 0; i < 6; i++)
            {
                game.WonPoint(points[i]);
                Assert.AreEqual(expected_scores[i], game.GetScore());
            }
        }
        
        [Test]
        public void CheckGame1()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithFirstComputeScoreStrategy();
            RealisticTennisGame(game);
        }
        
        [Test]
        public void CheckGame2()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithSecondComputeScoreStrategy();
            RealisticTennisGame(game);
        }

        [Test]
        public void CheckGame3()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithThirdComputeScoreStrategy();
            RealisticTennisGame(game);
        }
    }
}
