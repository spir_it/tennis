﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tennis.ComputeStrategies;

namespace Tennis.Tests
{
    [TestFixture(0, 0, "Love-All")]
    [TestFixture(1, 1, "Fifteen-All")]
    [TestFixture(2, 2, "Thirty-All")]
    [TestFixture(3, 3, "Forty-All")]
    [TestFixture(4, 4, "Deuce")]
    [TestFixture(1, 0, "Fifteen-Love")]
    [TestFixture(0, 1, "Love-Fifteen")]
    [TestFixture(2, 0, "Thirty-Love")]
    [TestFixture(0, 2, "Love-Thirty")]
    [TestFixture(3, 0, "Forty-Love")]
    [TestFixture(0, 3, "Love-Forty")]
    [TestFixture(4, 0, "Win for player1")]
    [TestFixture(0, 4, "Win for player2")]
    [TestFixture(2, 1, "Thirty-Fifteen")]
    [TestFixture(1, 2, "Fifteen-Thirty")]
    [TestFixture(3, 1, "Forty-Fifteen")]
    [TestFixture(1, 3, "Fifteen-Forty")]
    [TestFixture(4, 1, "Win for player1")]
    [TestFixture(1, 4, "Win for player2")]
    [TestFixture(3, 2, "Forty-Thirty")]
    [TestFixture(2, 3, "Thirty-Forty")]
    [TestFixture(4, 2, "Win for player1")]
    [TestFixture(2, 4, "Win for player2")]
    [TestFixture(4, 3, "Advantage player1")]
    [TestFixture(3, 4, "Advantage player2")]
    [TestFixture(5, 4, "Advantage player1")]
    [TestFixture(4, 5, "Advantage player2")]
    [TestFixture(15, 14, "Advantage player1")]
    [TestFixture(14, 15, "Advantage player2")]
    [TestFixture(6, 4, "Win for player1")]
    [TestFixture(4, 6, "Win for player2")]
    [TestFixture(16, 14, "Win for player1")]
    [TestFixture(14, 16, "Win for player2")]
    public class TennisTest 
    {
        private readonly int _player1Score;
        private readonly int _player2Score;
        private readonly string _expectedScore;
        private readonly TennisGameFactory _tennisGameFactory;

        public TennisTest(int player1Score, int player2Score, string expectedScore) : base()
        {
            _player1Score = player1Score;
            _player2Score = player2Score;
            _expectedScore = expectedScore;
            _tennisGameFactory = new TennisGameFactory(TennisGameDefaults.Player1Name, TennisGameDefaults.Player2Name);
        }

        [Test]
        public void CheckTennisGame1()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithFirstComputeScoreStrategy();
            CheckAllScores(game);
        }

        [Test]
        public void CheckTennisGame2()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithSecondComputeScoreStrategy();
            CheckAllScores(game);
        }

        [Test]
        public void CheckTennisGame3()
        {
            ITennisGameExtended game = _tennisGameFactory.GetTennisGameWithThirdComputeScoreStrategy();
            CheckAllScores(game);
        }

        public void CheckAllScores(ITennisGameExtended game)
        {
            game.WonPoints(TennisGameDefaults.Player1Name, _player1Score);
            game.WonPoints(TennisGameDefaults.Player2Name, _player2Score);

            Assert.AreEqual(this._expectedScore, game.GetScore());
        }

    }


}
