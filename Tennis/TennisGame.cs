﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tennis.ComputeStrategies;

namespace Tennis
{
    public class TennisGame : ITennisGameExtended
    {
        private readonly Player _player1;
        private readonly Player _player2;
        private readonly PointsBoard _pointsBoard;
        private readonly IComputeScoreStrategy _computeScoreStrategy;

        public TennisGame(string player1Name, string player2Name, IComputeScoreStrategy computeScoreStrategy)
        {
            _player1 = new Player(player1Name);
            _player2 = new Player(player2Name);
            _pointsBoard = new PointsBoard(_player1, _player2);
            _computeScoreStrategy = computeScoreStrategy;
        }

        public void WonPoint(string playerName)
        {
            _pointsBoard.AddPoint(playerName);
        }

        public void WonPoints(string playerName, int points)
        {
            for (int i = 0; i < points; i++)
            {
                WonPoint(playerName);
            }
        }

        public string GetScore()
        {
            return _computeScoreStrategy.ComputeScoreInWords(_player1, _player2);
        }

    }
}
