﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis.ComputeStrategies
{
    public interface IComputeScoreStrategy
    {
        string ComputeScoreInWords(Player player1, Player player2);
    }
}
