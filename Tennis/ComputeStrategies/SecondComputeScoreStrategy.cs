﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis.ComputeStrategies
{
    public class SecondComputeScoreStrategy : IComputeScoreStrategy
    {
        private ScoreBuilder _scoreBuilder;

        public SecondComputeScoreStrategy()
        {
            _scoreBuilder = new ScoreBuilder();
        }

        public string ComputeScoreInWords(Player player1, Player player2)
        {
            string score = String.Empty;

            if (ComputeCondition1(player1.Points, player2.Points))
            {
                _scoreBuilder.WithDrawScore(player1.Points.ToWord());
            }
            else if (ComputeCondition2(player1.Points, player2.Points))
            {
                _scoreBuilder.WithDeuceScore();
            }
            else if (ComputeCondition3(player1.Points, player2.Points))
            {
                _scoreBuilder.WithToLoveScore(player1.Points.ToWord());
            }
            else if (ComputeCondition3(player2.Points, player1.Points))
            {
                _scoreBuilder.WithToLoveScoreReverted(player2.Points.ToWord());
            }
            else if (ComputeCondition4(player1.Points, player2.Points))
            {
                _scoreBuilder.WithRegularScore(player1.Points.ToWord(), player2.Points.ToWord());
            }
            else if (ComputeCondition4(player2.Points, player1.Points))
            {
                _scoreBuilder.WithRegularScore(player1.Points.ToWord(), player2.Points.ToWord());
            }
            else if (ComputeCondition5(player1.Points, player2.Points))
            {
                _scoreBuilder.WithAdvantageScore(player1.Name);
            }
            else if (ComputeCondition5(player2.Points, player1.Points))
            {
                _scoreBuilder.WithAdvantageScore(player2.Name);
            }
            else if (ComputeCondition6(player1.Points, player2.Points))
            {
                _scoreBuilder.WithWinForScore(player1.Name);
            }
            else if (ComputeCondition6(player2.Points, player1.Points))
            {
                _scoreBuilder.WithWinForScore(player2.Name);
            }

            return _scoreBuilder.Build();
        }

        private bool ComputeCondition1(Point player1Point, Point player2Point) => player1Point.Value == player2Point.Value && player1Point.Value < 4;
        private bool ComputeCondition2(Point player1Point, Point player2Point) => player1Point.Value == player2Point.Value && player1Point.Value > 3;
        private bool ComputeCondition3(Point player1Point, Point player2Point) => player1Point.Value > 0 && player2Point.Value == 0;
        private bool ComputeCondition4(Point player1Point, Point player2Point) => player1Point.Value > player2Point.Value && player1Point.Value < 4;
        private bool ComputeCondition5(Point player1Point, Point player2Point) => player1Point.Value > player2Point.Value && player2Point.Value >= 3;
        private bool ComputeCondition6(Point player1Point, Point player2Point) => player1Point.Value >= 4 && player2Point.Value >= 0 && (player1Point.Value - player2Point.Value) >= 2;
    }
}
