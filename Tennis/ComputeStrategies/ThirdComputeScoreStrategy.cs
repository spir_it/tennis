﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Tennis.ComputeStrategies
{
    public class ThirdComputeScoreStrategy : IComputeScoreStrategy
    {
        private ScoreBuilder _scoreBuilder;

        public ThirdComputeScoreStrategy()
        {
            _scoreBuilder = new ScoreBuilder();
        }

        public string ComputeScoreInWords(Player player1, Player player2)
        {
            int player1Points = player1.Points.Value;
            int player2Points = player2.Points.Value;
            if (player1Points < 4 && player2Points < 4)
            {
                string player1Numeral = player1.Points.ToWord();
                string player2Numeral = player2.Points.ToWord();
                if (player1Points == player2Points)
                {
                    _scoreBuilder.WithDrawScore(player1Numeral);
                }
                else
                {
                    _scoreBuilder.WithRegularScore(player1Numeral, player2Numeral);
                }
            }
            else if (player1Points == player2Points)
            {
                _scoreBuilder.WithDeuceScore();
            }
            else
            {
                string winner = player1Points > player2Points ? player1.Name : player2.Name;
                if (Math.Abs(player1Points - player2Points) == 1)
                {
                    _scoreBuilder.WithAdvantageScore(winner);
                }
                else
                {
                    _scoreBuilder.WithWinForScore(winner);
                }
            }

            return _scoreBuilder.Build();
        }
    }
}
