﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis.ComputeStrategies
{
    public class FirstComputeScoreStrategy : IComputeScoreStrategy
    {
        private ScoreBuilder _scoreBuilder;

        public FirstComputeScoreStrategy()
        {
            _scoreBuilder = new ScoreBuilder();
        }

        public string ComputeScoreInWords(Player player1, Player player2)
        {
            if (player1.Points.Value == player2.Points.Value)
            {
                if (player1.Points.Value < 4)
                {
                    _scoreBuilder.WithDrawScore(player1.Points.ToWord());
                }
                else
                {
                    _scoreBuilder.WithDeuceScore();
                }
            }
            else if (player1.Points.Value >= 4 || player2.Points.Value >= 4)
            {
                int minusResult = player1.Points.Value - player2.Points.Value;
                string winner = minusResult > 0 ? player1.Name : player2.Name;
                if (Math.Abs(minusResult) == 1)
                {
                    _scoreBuilder.WithAdvantageScore(winner);
                }
                else
                {
                    _scoreBuilder.WithWinForScore(winner);
                }
            }
            else
            {
                _scoreBuilder.WithRegularScore(player1.Points.ToWord(), player2.Points.ToWord());
            }

            return _scoreBuilder.Build();
        }
    }
}
