﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public class ScoreBuilder
    {
        string _scoreInWords = String.Empty;

        public void WithRegularScore(string player1Numeral, string player2Numeral)
        {
            _scoreInWords = $"{player1Numeral}-{player2Numeral}";
        }

        public void WithDrawScore(string playersNumeral)
        {
            _scoreInWords = $"{playersNumeral}-All";
        }

        public void WithDeuceScore()
        {
            _scoreInWords = "Deuce";
        }

        public void WithAdvantageScore(string playerName)
        {
            _scoreInWords = $"Advantage {playerName}";
        }

        public void WithWinForScore(string playerName)
        {
            _scoreInWords = $"Win for {playerName}";
        }

        public void WithToLoveScore(string playersNumeral)
        {
            _scoreInWords = $"{playersNumeral}-Love";
        }

        public void WithToLoveScoreReverted(string playersNumeral)
        {
            _scoreInWords = $"Love-{playersNumeral}";
        }



        public string Build()
        {
            return _scoreInWords;
        }
    }
}
