﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
    public interface ITennisGameExtended : ITennisGame
    {
        void WonPoints(string playerName, int points);
    }
}
